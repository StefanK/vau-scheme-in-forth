
# Table of Contents

1.  [Purpose](#org18167b9)
2.  [Reasoning](#orgc125c7b)
3.  [Future work for this project or other projects](#org4d4aef2)
    1.  [regional garbage collector](#orgc9fecbc)
        1.  [regions for objects without pointers](#orgb60ec73)
        2.  [regions for objects with pointers](#org904b1f3)

In this project we implement a scheme interpreter in forth. We base it
on:

-   vau calculus of J. Shutt
    c.f. <https://web.cs.wpi.edu/~jshutt/kernel.html> and
    [The Revised -1 Report on the Kernel Programming Language (revised 29 October 2009)](https://ftp.cs.wpi.edu/pub/techreports/pdf/05-07.pdf)
-   CE[S]K-machines
-   delimeted continuations, c.f.
    [A Monadic Framework for Delimited Continuations](https://legacy.cs.indiana.edu/~dyb/pubs/monadicDC.pdf)

We use forth because of the minimal binary boot project, which
provides a forth system based on a tiny pre forth nucleus of 165 bytes
as a linux elf executable.


<a id="org18167b9"></a>

# Purpose

We want to provide an interpreter for the C compiler of
[GNU MES](https://www.gnu.org/software/mes/).


<a id="orgc125c7b"></a>

# Reasoning

-   We use vau calculus as a base, as this allows us
    1.  a tiny lisp interpreter,
    2.  to define syntactic forms using vau,
    3.  only need for primitives.
-   Delimited continuations are more general/efficient then call/cc.
-   CESK-machines are simple abstract models for interpreters.


<a id="org4d4aef2"></a>

# Future work for this project or other projects


<a id="orgc9fecbc"></a>

## regional garbage collector

parameters:

-   bitsPerAddressUnit &#x2013; mostly 8
-   bitsPerPointer     &#x2013; 64, 32 or 16
-   addressUnitsPerPage &#x2013; e.g. 0x400, 4 KiB, 4.096 Bytes


<a id="orgb60ec73"></a>

### regions for objects without pointers

1.  regions for objects using only a few bits

    parameters:

    -   objSize in bits
    -   regionSize in address units, where there exists an integer n
        such that: regionSize == (objSize \* n) / bitsPerAddressUnit

2.  regions for small objects

    I.e. the object is smaller than a pointer. Therefore, it cannot
    contain pointers itself. Not even forward information or mark
    bits. Thus, we must provide these separately.

    parameters:

    -   objSize in address units
    -   regionSize in address units

    data

    -   payload: addressUnit[regionSize]
    -   markBits: addressUnit[ceil(regionSize / bitsPerAddressUnit)]

3.  regions for objects without pointers

    parameters:

    -   objSize in address units
    -   regionSize in address units


<a id="org904b1f3"></a>

### regions for objects with pointers

1.  objects smaller than the memory page of the operating system

    parameters:

    -   objSize in address units
    -   regionSize in address units, where there exists an integer n
        such that: regionSize == objSize \* n

2.  objects bigger than the memory page of the operating system

    use directly the memory pages of the operating system
