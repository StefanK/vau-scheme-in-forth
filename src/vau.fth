\ 2023 Copyright (C) Dr. Stefan Karrmann
\ A vau interpreter in forth, either gforth or mbbforth
\ It works on machines with 16 bit words or bigger.

\ References:
\ R. Kent Dybvig, Simon Peyton Jones, Amr Sabry∗: A Monadic Framework for Delimited Continuations, https://legacy.cs.indiana.edu/~dyb/pubs/monadicDC.pdf
\ https://web.wpi.edu/Pubs/ETD/Available/etd-090110-124904/unrestricted/jshutt.pdf

\ We use pointer tagging with only two tag bits:
\ %01 for immediate number,
\ %10 for other atoms, i.e.some immediate values and complex data
\ %00 for pairs, the target looks like
\ +n--------10+n--------10+
\ | car     00| cdr     00|
\ | imm-int  1| cdr     00| immediate data in car
\ | infoPtr 10| data0..n  | n in 2*N
\ | fwdPtr  10| Ptr     00|
\ | valueP  10| xxxxxxxxxx| one char
\ +n--------10+n--------10+
\ and additional immediates for nil #t #f at top of infoPtr space.

\ ;; meta-circular evaluator for vau calculus
\ ;; page 52 in https://web.wpi.edu/Pubs/ETD/Available/etd-090110-124904/unrestricted/jshutt.pdf
\ (define (eval-rec exp env)
\   (cond
\    ((symbol? exp) (lookup-value exp env))
\    ((atom? exp) exp)
\    (else (apply (eval (car exp) env) (cdr exp) env))))

\ (define (eval-cps exp env cont)
\   (cond
\    ((symbol? exp) (lookup-value exp env cont))
\    ((atom?   exp) (cont exp))
\    (else (eval (car exp) env
\                (lambda (f)
\                  (apply f (cdr exp) env cont))))))
\ ; with trampoline, global state and gc-safe
\ (define (eval-loop)
\   (if (current>state)
\       (begin (eval-step) (eval-loop))
\       current))

\ ;; CESK-machine https://legacy.cs.indiana.edu/ftp/techreports/TR202.pdf
\ ;; state is (expr,env,store,pc,mc) with start (expr0,env0,0,0)
\ ;; we drop store and add [mode-]eval?, which replaces the (ret val)-continuation
\ ;; So, our state is:  (eval?,expr,env,pc,mc)
\ ;; we adapt the rules to vau-calculus:

\ ;; E(#t,x,env,pc,mc) = (#f,(env x),0,pc,mc) ; return value of variable
\ ;; E(#t,v,env,pc,mv) = (#f,v,0,pc,mc)       ; return (immediate) value
\ ;; E(#t,(M.Ns),e,pc,mc) = (#t,M,e,(Arg0 Ns e pc),mc)

\ ;; app:
\ ;; E(#f,(vprim f),e0,(Arg0 Ns e pc),mc) = (f) ; vau primitive
\ ;; E(#f,(vau fs body e1),e0,(Arg0 Ns e pc),mc)
\ ;;  = (#t,body,e1[fs := (e.Ns)],pc,mc)        ; vau abstraction
\ ;; E(#f,l,e0,(Arg0 Ns e pc),mc) = (#f,l,e0,(Args (().Ns) e pc),mc)

\ ;; args:
\ ;; E(#f,v,e0,(Args (vs.N.Ns) e pc),mc) = (#t,N,0,e,(Args ((v.vs).Ns) e pc),mc)
\ ;; E(#f,v,e0,(Args (vs.()) e pc),mc)
\ ;;   = (f')                                 if f == vprim f'
\ ;;   = (#t,body,e1[fs := (tail vs')],pc,mc) if f == (lambda fs body e1)
\ ;;   where vs' = (reverse (cons v vs)); f = (head fs'); f = vprim f'

\ First, we use the Cheney garbage collector.
\ We could also use the garbage collector of Baker for serial real time gc.

\ With only two bits for tagging we have no space in a word for the mark
\ bit for a mark-and-X garbage collector. Of course, we could use a
\ bitmap block. For n double words, we need n/bitsPerWord words.

\ The root set is state of the CEK-machine and some helper variables,
\ i.e. expr0, expr1, internals.
\ For simplicity internals is a list of the form (atom0 atom1 ...)


[IFDEF] scm-code
    scm-code
[ENDIF]

marker scm-code
[IFUNDEF] 3drop
    : 3drop ( x0 x1 x2 -- )
        2drop drop ;
[ENDIF]
[IFUNDEF] ndrop
    : ndrop ( xu .. x1 u -- ) \ drop u+1 stack items
     1+ cells sp@ + sp! ;
[ENDIF]
[IFUNDEF] cells/
    : cells/ cells / ;
[ENDIF]
[IFUNDEF] ptr%
    ' cell% alias ptr%
[ENDIF]
[IFUNDEF] int%
    ' cell% alias int%
[ENDIF]
[IFUNDEF] uint%
    ' cell% alias uint%
[ENDIF]
[IFUNDEF] alloc
    : alloc ( u -- a )
        allocate throw ;
[ENDIF]
[IFUNDEF] 0!
    : 0! ( a -- )
        0 swap ! ;
[ENDIF]
[IFUNDEF] cell/
    : cell/ ( u0 -- u1 )
        cell / ;
[ENDIF]
[IFUNDEF] odd?
    : even? ( x -- t/f )
        1 and 0= ;
    : odd? ( x -- t/f )
        1 and 0> ;
[ENDIF]
[IFUNDEF] =?
    : =? ( x0 x1 -- t | x0 f )
        over = dup IF nip THEN ;
[ENDIF]

#3 constant tag-mask
#0 constant tag-cxr
#1 constant tag-immediate-int
#2 constant tag-info

: get-tag ( lisp -- u:tag )
    #3 and ;

: untag ( lisp -- a:adr1 )
    [ tag-mask invert ]l and ;

tag-mask invert tag-info or dup constant #nil
tag-mask 1+ - dup constant immediate#t
tag-mask 1+ - dup constant immediate#f
tag-mask 1+ - dup constant immediate#inert
tag-mask 1+ - dup constant noEval
constant immediate#min


struct
    ptr% field info>scan     ( a:toSpace -- ; mem0 -- mem1 )
    ptr% field info>evacuate ( a:fromSpace -- a:toSpace )
    ptr% field info>apply    ( i*x a:obj env cont -- value cont )
    ptr% field info>display  ( a:obj -- )
    ptr% field info>equal?   ( a:obj0 a:obj1 -- t/f )
    uint% field info>size    ( -- u:size )
end-struct info%

struct
   ptr% field mem>toSpace-max
   ptr% field mem>toSpace
   ptr% field mem>fromSpace
   ptr% field mem>fromSpace-max
   ptr% field mem>scan
   ptr% field mem>free
end-struct mem%

create mem mem% %size allot
create info-forward info% %size allot

\ CESK state
true value eval?                        \ -1 eval 0 value
variable expr                           \ aka control
\ variable val                            \ replaces return continuation
variable env
variable cs                             \ continuation stack, i.e. listq
variable pc                             \ partial continuation
variable mc                             \ meta continuation
\ additional roots:
variable expr0
variable expr1
variable expr2
variable error-cont
variable interned                       \ interned values mostly symbols

[IFDEF] 0
    ' 0 alias enum-start
[ELSE]
    : enum-start 0 ;
[ENDIF]

immediate#f constant cont-args0         \ (args0 Ns env k)
\ todo: filename, lineno, column

( === 1. The garbage collector === )

#42 constant OOM                        \ out of memory

defer cheney-gc

: mem>hasSpace ( u -- t/f )
    2* cells  mem mem>free @ +   mem mem>toSpace-max < ;

: mem>alloc ( u:#pairs -- a:toSpace )
    2* cells
    dup mem>hasSpace 0= IF
        ." cheney gc startet." cr
        cheney-gc
        dup mem>hasSpace 0= IF OOM throw THEN
    THEN
    mem mem>free @ swap over + mem mem>free !
;

: evacuate-pair ( a:fromSpace0 -- a:toSpace )
    1 mem>alloc ( a:fromSpace0 a:toSpace )
    tuck [ 2 cells ]l cmove
    \ over @ over !
    \ swap cell+ @ over cell+ !
;

: evacuate ( a:?fromSpace -- a:toSpace )
    dup mem mem>fromspace @  mem mem>fromSpace-max @  within
    IF
        dup get-tag
        2 = IF \ infoPtr
            dup @ info>evacuate @ execute
        ELSE \ pair
            evacuate-pair
        THEN
    THEN
;
: scan-cxr ( a:adr0 -- a:adr1 )
    dup @ get-tag
    0= IF \ not integer but pointer
        dup @ ( a:adr0 a:adr-cxr )
        dup mem mem>fromSpace @ mem mem>fromSpace-max @
        within ( x a b -- f )
        IF \ fromSpace
            evacuate
        THEN
    THEN
    cell+
;

: evacuate-root ( a -- )
    \ evacuate and use forward pointer for update
    dup @ dup evacuate cell+ @ swap ! ;

:noname \ cheney-gc ( -- )
    \ flip
    mem mem>toSpace @
    mem mem>fromSpace @
    dup mem mem>free !
    dup mem mem>scan !
    mem mem>toSpace !
    mem mem>fromSpace !
    mem mem>toSpace-max @
    mem mem>fromSpace-max @
    mem mem>toSpace-max !
    mem mem>fromSpace-max !
    0 expr env pc mc expr0 expr1 expr2 interned
    BEGIN dup WHILE evacuate-root REPEAT
    BEGIN
        mem>scan @ dup mem>free @ < WHILE
            dup get-tag 2 = IF \ infoPtr
                @ tag-mask info>scan @ execute
            ELSE scan-cxr scan-cxr info>scan ! \ pair
            THEN
    REPEAT
; IS cheney-gc

: scan-info-default ( a:toSpace -- )
    \g toSpace: | info | ptr0 | ... | ptrN | N in 2*N
    \g                   +--- | info | data... |
    \g              or   +--- | car  | cdr |
    dup @ info>size @ ( a u )
    BEGIN dup WHILE
            1- swap cell+ dup @ evacuate
            over !                      \ update to toSpace
            swap
    REPEAT
;

: evacuate-info-default ( a:fromSpace -- a:toSpace )
    dup
    mem mem>free @  dup >r over @ info>size @ cells
    2dup + mem mem>free !               \ todo: use mem>alloc with
                                        \ cells not pairs
    ( a:fromSpace a:fromSpace a:toSpaceFree u:bytes ; R: a:toSpace )
    cmove
    info-forward over !
    r@ swap cell+ !  r>
;

: evacuate-info-forward ( a:fromSpace -- a:toSpace )
    cell+ @ ;
' evacuate-info-forward info-forward info>evacuate !

: scan-info-forward ( a:toSpace -- )
    ." scan forward in toSpace: Not allowed with Cheney's stop and go gc!" bye ;
' scan-info-forward info-forward info>scan !

( === basic pair functions  === )

'  @ alias car ( pair -- car )
: cdr      ( pair -- cdr )
    cell+ @ ;
' ! alias car! ( car pair -- )
: cdr! ( cdr pair -- )
    cell+ ! ;
: new-pair ( -- pair )
    1 mem>alloc ;
: cons ( -- pair )
    new-pair
    expr0 @ over car!
    expr1 @ over cdr!
;
: caar ( pair -- caar )
    @ @ ;
: cdar ( pair -- cdar )
    @ cell+ @ ;
: cadr ( pair -- cdar )
    cell+ @ @ ;
: cddr ( pair -- cdar )
    cell+ @ cell+ @ ;

' = alias eq? ( lisp0 lisp1 -- t/f )
: nil? ( lisp -- t/f )
    #nil = ;
: nil-not? ( lisp -- t/f )
    #nil <> ;
: pair? ( lisp -- t/f )
    get-tag 0= ;

: immediate-number? ( lisp -- t/f )
    tag-immediate-int and 0> ;
: atom? ( lisp -- t/f )
    get-tag 0<> ;
: info? ( lisp -- t/f )
    get-tag tag-info = ;

( ==== continuations suppot ==== )

: cont-args? ( -- t/f )
    pc @ car pair? ;
: cont-is? ( u -- t/f )
    pc @ car = ;
\ : cont-exit?  cont-exit  cont-is? ;
: cont-args0? cont-args0 cont-is? ;
\ : cont-argsN? cont-argsN cont-is? ;
\ : cont-if? cont-if cont-is? ;

: args0-drop
    pc @ cddr cdr pc ! ;
' args0-drop alias if-drop
: args0-args pc @ cadr ;               \ ( pc: args0 Ns env k -- Ns )
: args0-env pc @ cddr car ;            \ ( pc: args0 Ns env k -- env )
: args0to* \ ( pc: args0 Ns env k -- ((nil.Ns) env k)
    #nil expr0 !  pc @ cadr expr1 !  cons expr0 ! \ expr0: (nil.Ns)
    pc @ cddr expr1 ! cons pc !
;
: args-args pc @ cdar ;                   \ ( pc: args (vs.Ns) env k -- Ns
: args-next \ ( expr: v; pc: ((vs).N.Ns) env k -- ((v.vs).Ns) env k ; expr: N; env: env )
    expr @ expr0 !  pc @ caar expr1 !  cons expr0 ! \ (v.vs)
    pc @ cdar cdr expr1 ! cons expr0 !              \ ((v.vs).Ns)
    pc @ cdr expr1 ! cons                           \ ((v.vs).Ns) env k

    pc @ cdar car expr !
    pc @ cadr env !
    pc !
    #nil dup expr0 ! expr1 !
    true IS eval?
;
: args-final? \ ( pc: ((vs).Ns) env k) -- Ns null? )
    pc @ cdar nil? ;

: pre-lambda \ ( pc: ((vn-1..v0).nil) env pc -- pc:pc,env:env,expr:v0,expr1:(v1 ... vn) )
    pc @ caar #nil =? IF \ thunk
        #nil expr1 !
    ELSE
        expr @ expr0 !  #nil expr1 !  cons expr1 !
        pc @ caar  expr !
        BEGIN expr @ dup pair? WHILE
                car expr0 ! cons expr1 !
                expr @ cdr expr !
        REPEAT
        expr1 @ dup  car expr ! cdr expr1 !
    THEN
    #nil dup env ! expr0 !              \ avoid memory leaks
    pc @ cddr pc !
;

( === print - debug print in forth === )

defer print-tos

: print-list ( lisp:pair -- )
    [CHAR] ( emit
    BEGIN
        dup car print-tos
        cdr dup pair? WHILE space
    REPEAT
    dup nil-not? IF ."  . " print-tos
    ELSE drop
    THEN
    [CHAR] ) emit
;

: (print-tos) ( a:lisp -- )
    #nil          =? IF ." ()" exit THEN
    immediate#t  =? IF ." #t" exit THEN
    immediate#f  =? IF ." #f" exit THEN
    dup immediate-number? IF 2/ dec. exit THEN
    immediate#inert =? IF ." #inert" exit THEN
    dup pair? IF print-list exit THEN
    dup info? IF
        untag  dup @ info>display @ execute
        exit
    THEN
    ." unknown type to print" abort
;
' (print-tos) is print-tos

: print ( expr )
    expr @ print-tos ;

( === environment === )
\ quick and dirty as a rib without encapsulating, i.e. info structure:
\ ((vars . vals) . parent)

' #nil alias new-environment ( -- env )
' cons alias new-frame ( expr0:vars expr1:vals -- frame )
' cons alias add-frame ( expr0:frame expr1:parent-env -- new-env )

: extend-env ( expr0:var expr1:val env:e -- env:e' )
    ." extend-env:" cr
    cons dup                            \ (cons var val) dup
    .s cr
    env @ print-tos cr
    env @ caar swap cdr!                \ (cons var vars)
    env @ car car!                      \ env: ((cons vars' val) . parent)
    expr1 @ expr0 !                     \ val
    cons dup                            \ (cons val val) dup
    env @ cdar swap cdr!                \ (cons val vals)
    env @ car cdr!                      \ env: ((vars' . vals').parent)
;

: lookup-frame ( sym vars vals env -- a | 0 )
    BEGIN
      (  ." lookup-frame:"
        ." sym:" 3 pick dup hex. print-tos cr
        ." vars:" 2 pick dup print-tos cr
        dup pair? IF car hex. ELSE drop THEN cr
        ." vals:" 1 pick print-tos cr
        ." env:"  0 pick print-tos cr
)
        over nil? IF dup nil?
            \ ." vals nil" cr .s
            IF 4 ndrop 0 THEN             \ not found
            \ ." vals nil" cr .s
            >r 2drop r@ caar r@ cdar r> cdr
            \ ." vals nil" cr .s
        ELSE ( sym vars vals env )
            \ ." loop" .s cr
            3 pick 3 pick cdr eq?
            IF drop nip nip cell+ exit THEN
            3 pick 3 pick car
            \ ." l-eq?: " .s cr
            eq?
            IF drop nip nip exit THEN
            rot cdr rot cdr rot
        THEN
    AGAIN
;

: lookup-sym ( sym -- a|0 )
    ." lookup-sym:" dup hex. dup print-tos cr
    #nil dup env @ lookup-frame ;

: define! ( sym val env cont -- a:val cont )
    ." todo: define!"
    \ Hm.. todo: set error>cont to handle not found
;

( === closure === )
\ layout: | info-closure | formal-parameters | env | body |
struct
    ptr% field closure>info
    ptr% field closure>formals
    ptr% field closure>env
    ptr% field closure>body
end-struct closure%

create info-closure-vprim  info% %size allot
create info-closure-vau    info% %size allot
create info-closure-lambda info% %size allot
create info-closure-lprim  info% %size allot

: clo-type?  ( closure u:type -- t/f )
    \ ." clo-type? " dup hex.
    swap untag @ = ;
: vprim?  info-closure-vprim  clo-type? ;
: vau?    info-closure-vau    clo-type? ;
: lambda? info-closure-lambda clo-type? ;
: lprim?  info-closure-lprim  clo-type? ;

: vprim-display ( lisp.untagged -- )
    ." <<vprim> "
        closure>body @    ."  body: forth word at " hex.
    [CHAR] > emit
; ' vprim-display info-closure-vprim info>display !

: lprim-display ( lisp.untagged -- )
    ." <<lprim> "
         closure>body @    ."  body: forth word at " hex.
    [CHAR] > emit
; ' lprim-display info-closure-lprim info>display !
: vau-display ( lisp.untagged -- )
    ." <<vau> "
    dup closure>formals @ ." formals: " print-tos
    dup closure>env @     ."  env: "
    dup pair? IF caar THEN print-tos
        closure>body @    ."  body: " print-tos
    [CHAR] > emit
; ' vau-display info-closure-vau info>display !

: lambda-display ( lisp.untagged -- )
    ." <<lambda> "
    dup closure>formals @ ." formals: " print-tos
    dup closure>env @     ."  env: "
    dup pair? IF caar THEN  print-tos
        closure>body @    ."  body: " print-tos
    [CHAR] > emit
; ' lambda-display info-closure-lambda info>display !

( === symbol === )

\ layout: | info-symbol | octet-len | octets ... | pad |

create info-symbol info% %size allot

: symbol-len ( symbol -- u )
    cell+ @ ;

: symbol-scan ( a:toSpace -- )
    cell+ dup @ cell+ 1- cell/ 1+ cells + mem mem>scan !
; ' symbol-scan info-symbol info>scan !

: symbol-evacuate ( a:fromSpace -- a:toSpace )
    mem mem>free @ dup >r over
    symbol-len cell+ 1- cell/ 1+ 1+ cells dup >r cmove>
    r> mem mem>free +!  r>
; ' symbol-evacuate info-symbol info>evacuate !

: symbol? ( lisp -- t/f )
    tag-info 2dup and xor IF false exit THEN
    immediate#min over u< IF false exit THEN
    untag @ info-symbol = ;

: symbol-display ( lisp.untagged -- )
    cell+ dup @ swap cell+ swap type
; ' symbol-display info-symbol info>display !

: symbol-new ( d-txt -- lisp )
    dup [ 2 cells ]l naligned cell+ cell+ mem>alloc
    info-symbol over !
    2dup cell+ !                        \ octet-len
    ( a l lisp  -- lisp a to l )
    >r r@ cell+ cell+ swap
    cmove r>
    \ -rot 2 pick cell+ cell+ swap cmove
    tag-info or
    ." sym-new: "
    dup hex. dup print-tos
;

: prompt-new ( -- lisp )
    0 0 symbol-new ;
' symbol? alias prompt? ( lisp -- t/f ) \ we assume length 0

( === eval === )

\ Beware don't reuse continuations we need them for subCont or call/cc !
\ check if <nil env !> needed to avoid memory leaks.
: ?lookup
    expr @ dup symbol? IF
        ." eval-sym:" cr
        lookup-sym @  expr !
        ." todo: handle not found"
        #nil env !                        \ avoid memory leaks
        false IS eval?  true exit
    THEN drop false
;
: ?atom
    expr @ dup atom? IF  \ E(v,e,k,mc) = (v,0,k,mc)
        ." eval-atom:" cr
        #nil env !                        \ avoid memory leaks
        false IS eval?  true exit
    THEN drop false
;
: ?apply
    expr @ dup pair? IF            \ apply - eval function
        \ E((M.Ns),e,pc,mc) = (M,0,e,arg0(Ns,e,pc),mc)
        ." eval-pair i.e. apply:" cr
        pc @ expr1 !  env @ expr0 !  cons expr1 !
        dup cdr expr0 ! cons expr1 !
        cont-args0 expr0 ! cons pc !
        car expr ! true exit
    THEN drop false
;
: ?eval-core ( -- t/f )
    eval? IF
        ?lookup dup ?exit drop
        ?atom   dup ?exit drop
        ?apply  dup ?exit drop
        ." unknown expr" expr @ print-tos abort
    THEN false
;
: ?mc ( -- t/f )
    pc @ nil? IF                           \ empty pc -> check mc
        mc @  BEGIN dup prompt? WHILE cdr REPEAT
        #nil =? IF 1 IS eval? true  exit THEN   \ empty mc -> done
        dup cdr mc !  car pc !
    THEN false
;
: ?vau
    expr @ dup vau? IF
        ." e-val vau:"
        \ E((vau (e.ps)e0.b),e1,(args0 Ns e2 k)) = (b,e0[(e.ps):=(e2.Ns)],k)
        pc @ cddr car  expr0 !  \ e2
        ." expr0: " expr0 @ print-tos cr
        pc @ cadr      expr1 !
        ." expr1: " expr1 @ print-tos cr
        cons expr1 ! \ (e2.Ns)
        ." expr1: " expr1 @ print-tos cr
        dup untag closure>formals @ expr0 !
        ." expr0: " expr0 @ print-tos cr
        new-frame expr0 ! \ ((e.ps).(e2.Ns))
        ." expr0: " expr0 @ print-tos cr
        dup untag closure>env @ expr1 ! add-frame env !
        untag closure>body @ expr !
        ." expr: " expr @ print-tos cr
        pc @ cddr cdr pc !
        true IS eval?  true exit
    THEN drop false
;
: ?vprim
    expr @ dup vprim? IF
        ." e-val vprim:"
        \ E(0,(vprim.f),e0,(args0 Ns e1 k)) = (f Ns e1 k)
        \ expr0 == Ns, expr1 == e1, pc == k
        pc @ cdr dup car expr0 !           \ Ns
        ." expr0: " expr0 @ print-tos cr
        cdr dup car expr1 !                \ e1
        ." expr1: " expr1 @ print-tos cr
        cdr pc !                           \ k
        ." pc: " pc @ print-tos cr
        ." expr: " expr @ print-tos cr
        untag closure>body @ dup hex. cr
        execute  true exit
    THEN drop false
;
: ?arg0 ( -- t/f )
    pc @ car dup cont-args0? IF
        ?vau     dup ?exit drop
        ?vprim   dup ?exit drop
        args0to* true
    THEN drop false
;
: ?lambda
    expr @ untag dup lambda? IF
        ." e-val lambda:"
        \ E(v,e0,args(((vs).#nil),e1,k) = (b,eL[xs:=(tail vs')],k)
        \   where vs' = (reverse (cons* v vs)), lambda xs eL b = (head vs')
        dup closure>body @ expr !
        dup closure>env  @ env !
        closure>formals @ expr0 ! new-frame expr0 !
        env @ expr1 ! add-frame env !
        true IS eval?  true exit
    THEN drop false
;
: ?lprim
    expr @ untag dup lprim? IF
        ." e-val lprim:"
        \ E(v,e0,args(((vs).#nil),e1,k) = (f real-world)
        \   where vs' = (reverse (cons* v vs)), lprim f = (head vs')
        \ pc == k, expr1 == vs'
        closure>body @ execute  true exit
    THEN drop false
;
: ?args ( -- t/f )
    pc @ car dup cont-args? IF
        \ E(v,e0,args(((vs).N.Ns),e1,k) = (N,e1,args(((v.vs).Ns,e1,k)
        args-args pair? IF args-next true exit THEN
        pre-lambda
        ." which lambda?"
        expr @ dup hex. print-tos cr
        expr0 @ dup hex. print-tos cr
        expr1 @ dup hex. print-tos cr
        expr @ untag @ hex. cr
        info-closure-lambda hex. cr
        info-closure-lprim hex. cr
        ?lambda dup ?exit drop
        ?lprim  dup ?exit drop
        ." unknown lambda like closure" print-tos cr abort
    THEN drop false
;
: eval-step ( -- )
    ." eval-step:" cr
    ." eval?: " eval? hex. cr
    ." expr: " print cr
    ." env: " env @ print-tos cr
    ." pc:  " pc @ print-tos cr
    ?eval-core ?exit
    ?mc ?exit                      \ inspect continuations pc and mc
    ?arg0 ?exit
    ?args ?exit
    \ todo: replace by primitives:
    \ pc @
    \ dup cont-if? IF
    \     \ E(#t,e0,('if (t e) e1 k)) = (t,e1,k)
    \     \ E(#f,e0,('if (t e) e1 k)) = (e,e1,k)
    \     cdr dup car  expr @ immediate#t =
    \     IF car ELSE cadr THEN  expr !
    \     cdr dup car env !
    \     cdr pc !
    \     true IS eval?  exit
    \ THEN
    \ dup cont-contTo IF
    \     \ E(v,e0,(contTo k0 k)) = (v,0,k0)
    \     pc @ cadr  pc !  exit
    \ THEN
    \ dup cont-exit? IF exit THEN

    hex. ." unknown continuation" abort
;

: eval-loop ( -- )     \ trampoline for eval-step
    BEGIN eval-step eval? 1 = UNTIL ;

: prim-read-char ;
: prim-read-token \ ( -- d 0 | '(' | ')' | d-txt 1 )
    \ ( ) intn string ...
;

: prim-read ( -- )
    \ (_,v,s,k)
    prim-read-char
    \ [CHAR] ( =? IF read-list-tail exit THEN
;

: read
;

: repl
    BEGIN
        \ global-env env !
        read expr !
        eval-loop
        print
    AGAIN
;

' str= alias octet= ( d-txt0 d-txt1 -- t/f )

: sym->d-txt ( l.untagged -- d-txt )
    cell+ dup cell+ swap @ ;

: equal-sym? ( l0 l1 -- t/f )
    2dup eq? IF 2drop true exit THEN
    dup symbol? 0= IF 2drop false exit THEN
    untag swap untag
    over @ over @ <> IF 2drop false exit THEN
    dup @ info-symbol <> IF 2drop false exit THEN
    sym->d-txt rot sym->d-txt octet=
;

: intern ( l0:val -- l1:val )
    interned @
    BEGIN dup pair? WHILE
            2dup car equal-sym?
            IF car nip exit THEN
            cdr
    REPEAT drop
    expr0 ! interned @ expr1 ! cons dup interned !
    car
  (  ." interned:"
    dup dup hex. print-tos cr
    )
;

: make-scheme ( u.mem -- )
    dup alloc
    dup mem mem>free !
    dup mem mem>toSpace !
    over + mem mem>toSpace-max !
    dup alloc
    dup mem mem>fromSpace !
    + mem mem>fromSpace-max !
    true is eval?
    0 expr env pc mc expr0 expr1 expr2 interned
    BEGIN dup WHILE #nil swap ! REPEAT drop
    \ S" vau" symbol-new expr0 !
    \ #nil expr1 ! cons  interned !
;

." init scheme:"
#100 make-scheme
.s cr

( === basic library === )

: lprim->closure ( xt -- lisp )
    [ closure% %size 2 cells / ]l mem>alloc
    info-closure-lprim over closure>info !
    #nil over closure>formals !
    #nil over closure>env !
    tuck closure>body !
    tag-info or
;

: vprim->closure ( xt -- lisp )
    [ closure% %size 2 cells / ]l mem>alloc
    info-closure-vprim over closure>info !
    #nil over closure>formals !
    #nil over closure>env !
    tuck closure>body !
    tag-info or
;

: lambda->closure ( expr0:lbd expr1:env -- lisp )
    [ closure% %size 2 cells / ]l
    mem>alloc
    info-closure-lambda over closure>info !
    expr1 @ over closure>env !
    expr0 @ cadr over closure>formals !
    expr0 @ cddr car over closure>body !
    tag-info or
;

( ===  primitives === )

: def-prim ( xt d-txt xt:clo -- )
    >r symbol-new intern expr0 !
    r> execute expr1 !
    extend-env
;
: def-vprim ['] vprim->closure def-prim ;
: def-lprim ['] lprim->closure def-prim ;

( ==== vau primitives ==== )
\ vprim: expr0 == Ns, expr1 == e1, pc == k

: vprim-vau
    \ E(vprim-vau,arg0(Ns,e1,k)) = (clo-vau((e2.x) e0 M),0,k) where Ns = (x e2 M)
    closure% %size mem>alloc
    info-closure-vau over closure>info !
    expr1 @ over closure>env !
    expr0 @ cddr car  over closure>body !
    tag-info or  expr !
    expr0 @ car expr1 !  expr0 @ cadr expr0 ! cons
    expr @ untag closure>formals !
    \ false IS eval?
;
\ create first, empty frame in environment:
#nil dup expr0 !  expr1 ! new-frame expr0 !
env @ expr1 ! add-frame env !
\ insert first function:
' vprim-vau S" vau" def-vprim

( ==== lambda primitives === )
\ lprim: pc == k, expr = v0, expr1 == (v1 .. vn)

: lprim>arg1 expr1 @ car ;
: lprim>arg2 expr1 @ cadr ;
: lprim>arg3 expr1 @ cddr car ;
: lprim>arg4 expr1 @ cddr cadr ;

: lprim-list
    expr1 @ expr ! ;
' lprim-list S" list" def-lprim

: lprim-cons
    \ lprim>arg1 expr0 ! lprim>arg2 expr1 ! cons expr !
    expr1 @ dup  car expr0 !  cadr expr1 !  cons expr ! ;
' lprim-cons S" cons" def-lprim

\ We can bootstrap define by:
\ (define-l ((vau (x) e x) quote) ((vau x e e)) (vau (x) e x))
\ (define-l (quote get-env) ((vau x e e)) (vau x e e))
\ (define-l (quote define) (get-env) (vau (p val) e (define-l p e (eval val e)))
\ ... (define-l (quote define-var-tree) (vau (param-tree val-tree) e ...))
: lprim-define-l \ (define-l sym env value)
    ." def-l: " cr
    lprim>arg1 print-tos
    lprim>arg2 print-tos
    lprim>arg3 print-tos
    lprim>arg1 expr0 !
    lprim>arg2 env !
    lprim>arg3 expr1 !
    extend-env
    immediate#inert expr !
; ' lprim-define-l S" define-l" def-lprim

: lprim-eval
    \ E(vn,e0,args(((vs.#nil),e1,k)) = (M,0,N,k)
    \ (eval expr env)
    lprim>arg2 env !  lprim>arg1 expr !
    true IS eval?
; ' lprim-eval S" eval" def-lprim

." env: " env @ print-tos

: lprim-qset!
    \ (qset! sym val e)
    lprim>arg3 env !
    lprim>arg2
    lprim>arg1
    lookup-sym dup IF ! exit THEN
    ." todo: handle not found" cr
;
: lprim-qdefined?
    \ (qdefined? sym e)
    lprim>arg2 env !
    lprim>arg1 lookup-sym
    IF immediate#t         \ found
    ELSE immediate#f
    THEN expr !
;

: lprim-pushPrompt
    \ (withPromt p 'e env)
    \ ;; E(#f,*,0,pc,mc) = (#t,e,env,#nil,(p.pc.mc))
    lprim>arg3 env !
    pc @ expr0 ! mc @ expr1 ! cons expr1 !
    lprim>arg1 expr0 ! cons mc !
    #nil pc !
    lprim>arg2 expr !
    true IS eval?
; ' lprim-pushPrompt S" pushPrompt" def-lprim

: splitAt ( p ls -- ls0 ls1 )
    swap >r
;
: append ( ls0 ls1 -- ls2 )
;

: lprim-withSubCont
    \ (withSubCont p f env)
    \ ;; E(#f,*,0,pc,mc) = (#t,(f (pc.mcBefore_p)),env,#nil,mcAfter_p)
;
: lprim-pushSubCont
    \ (withSubCont k 'e env)
    \ ;; E(#f,*,0,pc,mc) = (#t,e,env,#nil,(append k (pc.mc)))
;

\ : lprim-contTo
\     \ ( E(0,k0,e,args(((v lprim-contTo)),e1,k) = (0,v,0,k0) )
\     expr1 @ cdr dup cadr pc !  car expr !  ;
\     \ old( E(0,k0,e,args(((lprim contTo)),e1,k) = (0,v,0,(contTo k0 k)) )
\ \    expr1 @ cadr expr !
\ \    expr1 @ cddr car  pc !

\ : lprim-call/cc \ (call/cc f _k_) => (f (lambda (v _k0_) (contTo v _k_))
\     \ E(0,fv,0,args((lprim call/cc).#nil,e1,k))
\     \ = (0,(<lambda> v (env: k:=k), contTo v k),0,args((fv),0,k))
\     \ vs' = (reverse (cons* v vs)), f = (cadr vs')
\     \ (call/cc f) => (f <special lambda>)
\     lprim>arg1 expr0 !  #nil expr1 ! cons expr0 !
\     cons expr !
\     #nil expr0 !  pc @ expr1 !  cons expr1 !
\     expr @ expr0 !  cons pc !

\     \ #nil expr1 !  val @ expr0 ! cons expr0 ! cons val !
\     \ #nil expr0 !  pc @ cddr expr1 ! cons expr1 !
\     \ val @ expr0 ! cons pc !

\     \ (lambda (v) (contTo v k0))
\     closure% %size cells/ 2/ mem>alloc
\     info-closure-lambda  over closure>info !
\     dup closure>formals 0!
\     dup closure>env 0!
\     dup closure>body 0! expr !
\     S" v" 2dup 2>r intern expr0 !  #nil expr1 ! cons
\     expr @ closure>formals !
\     #nil env !
\     S" k" 2dup 2>r intern expr0 !  pc @ cddr expr1 ! extend-env
\     expr @ closure>env !
\     2r> intern expr0 !  #nil expr1 !  cons expr1 !
\     2r> intern expr0 !  cons expr1 !
\     ['] lprim-contTo lprim->closure expr0 ! cons
\     expr @ closure>body !
\ ;


: lprim-+-int ;
: lprim---int ;
: lprim-*-int ;
: lprim-/-int ;



: dbgIt
    ." symbol: "
    untag
    ." untagged:" .s cr
    cell+ dup @
    .s
    swap cell+ swap type cr
;

\ #nil print-tos
\ #nil nil? hex.
\ immediate#t print-tos
\ immediate#f print-tos
\ immediate#inert print-tos
\ 3 print-tos
\ -3 print-tos
\ ." info-symbol = " info-symbol hex. cr
\ S" test-sym"  symbol-new  .s print-tos

: test_quote ( -- lisp )
    \ quote, i.e.: (vau (x) e x)
    S" x" symbol-new intern expr0 !  #nil expr1 !  cons expr1 ! \ (x)
    #nil expr0 ! cons expr1 ! \ nil as formal parameter, which is never used
    \ S" e" symbol-new intern expr0 ! cons expr !
    expr1 @ cdr expr0 ! cons expr ! \ ((x) () x)
    print cr
    S" vau" symbol-new intern expr0 !
    expr @ expr1 ! cons expr0 ! \ (vau (x) () x)
    #nil expr1 ! cons expr1 !
    #nil expr0 ! cons expr !                       \ (() (vau (x) () x))
    print cr
    S" e" symbol-new intern expr0 ! #nil expr1 ! cons expr1 !
    expr1 @ car expr0 ! cons expr1 !    \ (e e)
    expr @ cadr cadr car expr0 ! cons expr1 !
    expr @ cadr car expr0 ! cons expr0 !
    #nil expr1 ! cons expr @ car!       \ (((vau x e e)) (vau (x) e x))
    print cr
    #nil expr0 ! expr @ expr1 ! cons expr ! \ (() ((vau x e e)) (vau (x) e x))
    print cr
    S" quote" symbol-new intern expr0 !  #nil expr1 ! cons expr1 !
    expr @ cddr car expr0 ! cons expr @ car!
    print cr
    S" define-l" symbol-new intern expr0 !
    expr @ expr1 ! cons expr !

    \ cons expr !          \ ((vau (x) () x) quote)
    \ expr @ car expr0 ! #nil expr1 ! cons expr1 !
    \ expr @ expr0 ! cons expr !
    \ S" define-l" symbol-new intern expr0 !
    \ expr @ expr1 ! cons expr !
    \ \ expr @ car expr !
    print cr
     \ todo: (ldefine ((vau (x) e x) quote) ((vau x e e)) (vau (x) e x))

    \ S" vau" symbol-new intern expr0 ! #nil expr1 ! cons env !
    \ interned @ print-tos cr
    \ ['] vprim-vau vprim->closure expr0 ! #nil expr1 ! cons expr1 !
    \ env @ expr0 ! cons expr0 ! #nil expr1 ! cons env !
    eval-loop
    print
;

test_quote print


\ Local Variables:
\ mode: forth
